//
//  NewRaulFeatureConfigurator.swift
//  testRaul
//
//  Created by Raul Moreno on 2/9/22.
//

import Foundation
import NewRaulFeature

struct NewRaulFeatureConfigurator {
    static func prepareFeature() -> RaulFeature {
        RaulFeature(dependencies: RaulDependenciesImpl())
    }
    
}

class RaulDependenciesImpl: RaulDependencies {
    let someText: String = "raulText"
}
