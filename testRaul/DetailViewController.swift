//
//  DetailViewController.swift
//  testRaul
//
//  Created by Aleix Cos on 20/5/22.
//

import UIKit

protocol DetailViewControllerDelegate: AnyObject {
    func didClickButton(text: String)
}
class DetailViewController: UIViewController {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var button: UIButton!

    var info: String = ""
    weak var delegate: DetailViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        print("detailViewControllerViewDidLoad")

        label.text = info

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("detailViewControlleWwillAppear")
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("detailViewControllerDidAppear")
        view.backgroundColor = .brown
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("detailViewControllerWillDisappear")
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("detailViewControllerDidDisappear")
    }

    deinit {
        print("detailViewControllerDeinit")
    }
    

    @IBAction func onClick(_ sender: Any) {
        delegate?.didClickButton(text: "comeBackData!")
        NotificationCenter.default.post(name: .detailNotification, object: nil)
        navigationController?.popViewController(animated: false)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension NSNotification.Name {
    static var detailNotification = NSNotification.Name("detailNotification")
}
