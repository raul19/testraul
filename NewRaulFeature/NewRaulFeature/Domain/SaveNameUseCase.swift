//
//  SaveNameUseCase.swift
//  NewRaulFeature
//
//  Created by Raul Moreno on 4/11/22.
//

import Foundation

protocol LocalStorageUseCase {
    func saveName(_ text: String)
    func retrieveName() -> String
}

final class LocalStorageUseCaseImpl {
    
    enum Constants {
        static let saveNameKey = "saveNameKey"
    }
    
    private let userDefaults: UserDefaults
    
    init(userDefaults: UserDefaults) {
        self.userDefaults = userDefaults
    }
}

extension LocalStorageUseCaseImpl: LocalStorageUseCase {
    func saveName(_ text: String) {
        userDefaults.set(text, forKey: Constants.saveNameKey)
        userDefaults.synchronize()
    }

    func retrieveName() -> String {
         return userDefaults.string(forKey: Constants.saveNameKey) ?? ""
    }
}
