//
//  ProfileView.swift
//  NewRaulFeature
//
//  Created by Raul Moreno on 18/11/22.
//

import SwiftUI

struct StampcardDetailView: View {
    struct ViewData {
        let blockView: BlockView.ViewData
    }
    
    var viewData: ViewData
    
    var body: some View {
        ScrollView {
            VStack(spacing: 0) {
                HStack(spacing: 0) {
                    BlockView(viewData: viewData.blockView)
                    BlockView(viewData: viewData.blockView)
                }
                .padding(.horizontal, 16)
            }
        }
        
    }
}

struct BlockView: View {
    struct ViewData {
        let image: (url: String, color: Color)
        let titleText: String
        let subTitleText: String
    }
    
    var viewData: ViewData
    
    var body: some View {
        VStack {
            ZStack {
                Circle()
                    .frame(width: 24, height: 24)
                    .foregroundColor(viewData.image.color)
                Image(
                    viewData.image.url,
                    bundle: Bundle(for:NewRaulViewController.self)
                )
                    .resizable(resizingMode: .stretch)
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 12, height: 12)
            }
            Text(viewData.titleText).font(.title).bold()
            Text(viewData.subTitleText).foregroundColor(.gray).font(.body)
            
        }
        .padding(.horizontal, 16)
        .padding(.vertical, 32)
        .overlay(
            RoundedRectangle(cornerRadius: 2)
                .stroke(Color.gray, lineWidth: 1)
        )
    }
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        StampcardDetailView(
            viewData: .init(
                blockView: .init(
                    image: ("oliva", .blue),
                    titleText: "2/6",
                    subTitleText: "To obtain a participation"
                )
            )
        )
    }
}
