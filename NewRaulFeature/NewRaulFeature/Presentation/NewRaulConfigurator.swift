//
//  NewRaulConfigurator.swift
//  NewRaulFeature
//
//  Created by Raul Moreno on 2/9/22.
//

import Foundation
import UIKit

extension NewRaulViewController {
    static func instantiate(dependencies: RaulDependencies) -> NewRaulViewController {
        let localStorage = LocalStorageUseCaseImpl(userDefaults: .standard)
        let presenter = NewRaulPresenter(localStorageUseCase: localStorage, someText: dependencies.someText)
        let viewController = NewRaulViewController(presenter: presenter)
        presenter.view = viewController
        return viewController
    }
}
