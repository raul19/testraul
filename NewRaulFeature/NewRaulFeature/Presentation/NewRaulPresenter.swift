//
//  NewRaulPresenter.swift
//  NewRaulFeature
//
//  Created by Raul Moreno on 2/9/22.
//

import Foundation

protocol NewRaulPresenterInterface {
    func didTapButton(text: String)
    func viewReady()
}

class NewRaulPresenter {
    
    weak var view: NewRaulViewInterface?
    private let someText: String
    private let localStorageUseCase: LocalStorageUseCase
    
    init(
        localStorageUseCase: LocalStorageUseCase,
        someText: String
    ) {
        self.someText = someText
        self.localStorageUseCase = localStorageUseCase
    }
    
}

extension NewRaulPresenter : NewRaulPresenterInterface {
    
    func didTapButton(text: String) {
        localStorageUseCase.saveName(text)
    }
    
    func viewReady() {
        view?.configureUi(
            viewData: NewRaulViewController.ViewData(
                title: someText,
                savedText: localStorageUseCase.retrieveName()
            )
        )
    }
}
