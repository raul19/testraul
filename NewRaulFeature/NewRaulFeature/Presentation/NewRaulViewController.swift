//
//  NewRaulViewController.swift
//  NewRaulFeature
//
//  Created by Raul Moreno on 2/9/22.
//

import Foundation
import UIKit

protocol NewRaulViewInterface : AnyObject {
    func configureUi(viewData: NewRaulViewController.ViewData)
}

class NewRaulViewController : UIViewController {
    
    struct ViewData {
        let title: String
        let savedText: String
    }
    
    private lazy var savedLabelTopConstraint = NSLayoutConstraint()
    private let label = UILabel()
    private let savedLabel: UILabel = {
       let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .brown
        label.font = .systemFont(ofSize: 12)
        return label
    }()
    private let button: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Click me", for: .normal)
        button.setTitleColor(.darkGray, for: .normal)
        button.setTitleColor(.green, for: .highlighted)
        button.addTarget(self, action: #selector(didTapButton), for: .touchUpInside)
        return button
    }()
    private let presenter: NewRaulPresenterInterface
    
    init(presenter: NewRaulPresenterInterface) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        addViews()
        addConstraints()
        presenter.viewReady()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension NewRaulViewController: NewRaulViewInterface {
    func configureUi(viewData: ViewData) {
        label.text = viewData.title
        savedLabel.text = viewData.savedText
    }
}

private extension NewRaulViewController {
    func addViews() {
        label.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(label)
        view.addSubview(button)
        view.addSubview(savedLabel)
    }
    
    func addConstraints() {
        savedLabelTopConstraint = savedLabel.topAnchor.constraint(equalTo: button.bottomAnchor, constant: 10)
        NSLayoutConstraint.activate([
            label.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            label.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
        NSLayoutConstraint.activate([
            button.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            button.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 10)
        ])
        NSLayoutConstraint.activate([
            savedLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            savedLabelTopConstraint
        ])
    }
    
    @objc func didTapButton() {
        presenter.didTapButton(text: button.titleLabel?.text ?? "")
        
        
        
        UIView.animate(withDuration: 2, delay: 0, options: .autoreverse) {
            self.view.backgroundColor = UIColor(red: 1, green: 87/255, blue: 51/255, alpha: 1)
            self.label.alpha = 0
            self.savedLabel.transform = CGAffineTransform(translationX: 0, y: 100)
        }
    }
}
