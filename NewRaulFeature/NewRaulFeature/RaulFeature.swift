//
//  RaulFeature.swift
//  NewRaulFeature
//
//  Created by Raul Moreno on 2/9/22.
//

import Foundation
import UIKit

public protocol RaulDependencies {
    var someText: String { get }
}

public struct RaulFeature {
    
    private let raulDependencies: RaulDependencies
    
    public init(dependencies: RaulDependencies) {
        self.raulDependencies = dependencies
    }
    
    public func prepareRaul() -> UIViewController {
        NewRaulViewController.instantiate(dependencies: raulDependencies)
    }
}
